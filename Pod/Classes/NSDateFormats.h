//
//  NSDateFormats.h
//  mobileclient
//
//  Created by Filip Duvnjak on 7/11/16.
//  Copyright © 2016 salesperformer.net. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DICTIONARY_DATE_FORMAT @"YYYY MM dd"
#define DICTIONARY_YEAR_FORMAT @"YYYY"
#define DICTIONARY_MONTH_FORMAT @"MM"
#define DICTIONARY_DAY_FORMAT @"dd"

#define FULL_MONT_NAME_YEAR_FORMAT @"MMMM dd, YYYY"
#define FULL_MONT_NAME_FORMAT @"MMMM dd"
