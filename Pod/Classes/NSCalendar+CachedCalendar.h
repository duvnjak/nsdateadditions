//
//  NSCalendar+CachedCalendar.h
//  mobileclient
//
//  Created by Filip Duvnjak on 5/3/14.
//  Copyright (c) 2014 salesperformer.net. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (CachedCalendar)

+ (NSCalendar *)currentCalendarCached;

@end
