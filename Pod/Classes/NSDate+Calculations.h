//
//  NSDateAddition.h
//  mobileclient
//
//  Created by Tobias Konsek on 10/8/11.
//  Copyright 2011 salesperformer.net. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    Minute,
    Hour,
    Day,
    Month,
    Year
}NSDateKindOfTime;

@interface NSDate(Calculations)

- (NSInteger) getMinute;
- (NSInteger) getHour;
- (NSInteger) getDay;
- (NSInteger) getMonth;
- (NSInteger) getYear;
- (NSInteger) getWeekday;
- (NSInteger) oneBasedWeekday;
- (NSInteger) getTime:(NSDateKindOfTime)kind;
- (NSInteger) numberOfDaysInMonth;

- (BOOL) isToday;
- (BOOL) isSameMonthAs:(NSDate*)date;
- (BOOL) isSameDayAs:(NSDate*)date;
- (BOOL) isSameYearAs:(NSDate*)date;
- (BOOL) isBetween:(NSDate*)date andDate:(NSDate*)otherDate;
- (BOOL) isStartOfDay;
- (BOOL) isEndOfDay;
- (BOOL) isBeforeDate:(NSDate *)date;
- (BOOL) isAfterDate:(NSDate *)date;

- (NSDate *) dateWithWeekDay:(NSInteger)weekDay;

- (NSDate *) getTomorrow;
- (NSDate *) getYesterday;
- (NSDate *) getPreviousMonth;
- (NSDate *) getNextMonth;
- (NSDate *) dateWithHour:(NSInteger)hour andMinuten:(NSInteger)minute;
- (NSDate *) dateWithHour:(NSInteger)hour;
- (NSDate *) dateWithMinute:(NSInteger)minute;
- (NSDate *) startOfDay;
- (NSDate *) startOfMonth;
- (NSDate *) startOfYear;
- (NSDate *) endOfDay;
- (NSDate *) findMondayOfCurrentWeek;
- (NSDate *) setDayOfMonth:(NSInteger)dayOfMonth;
- (NSDate *) setMonth:(NSInteger)month;
- (NSDate *) getMinuteWithOffset:(NSInteger)offset;
- (NSDate *) getHourWithOffset:(NSInteger)offset;
- (NSDate *) getDayWithOffset:(NSInteger)offset;
- (NSDate *) getWeekWithOffset:(NSInteger)offset;
- (NSDate *) getMonthWithOffset:(NSInteger)offset;
- (NSDate *) getYearWithOffset:(NSInteger)offset;

/* Static Methods */

+ (NSDate *) dateFromDay:(NSInteger)day andMonth:(NSInteger)month andYear:(NSInteger)year;
+ (NSDate *) earlierDateOf:(NSDate*)date andDate:(NSDate*)dateTwo;
+ (NSDate *) laterDateOf:(NSDate*)date andDate:(NSDate*)dateTwo;
+ (NSInteger) oneBasedWeekday:(NSInteger)weekday;
+ (NSInteger) midnightsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger) daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
+ (NSInteger) minutesBetweenDates:(NSDate *)firstDate andDate:(NSDate *)secondDate;

@end
