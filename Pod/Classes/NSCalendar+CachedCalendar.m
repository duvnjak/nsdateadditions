//
//  NSCalendar+CachedCalendar.m
//  mobileclient
//
//  Created by Filip Duvnjak on 5/3/14.
//  Copyright (c) 2014 salesperformer.net. All rights reserved.
//

#import "NSCalendar+CachedCalendar.h"

static NSCalendar *cachedCalendar;

@implementation NSCalendar (CachedCalendar)

+ (NSCalendar *)currentCalendarCached;
{
    if (!cachedCalendar) {
        cachedCalendar = [NSCalendar currentCalendar];
    }
    return cachedCalendar;
}

@end
