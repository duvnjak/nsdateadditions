//
//  NSDate+Constants.h
//  Near Lock
//
//  Created by Filip Duvnjak on 5/14/16.
//  Copyright © 2016 Oltica. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const k1MinuteInSeconds;
extern NSInteger const k3MinutesInSeconds;
extern NSInteger const k5MinutesInSeconds;
extern NSInteger const k10MinutesInSeconds;
extern NSInteger const k15MinutesInSeconds;
extern NSInteger const k30MinutesInSeconds;
extern NSInteger const k1HourInSeconds;
extern NSInteger const k1DayInSeconds;
extern NSInteger const k2DaysInSeconds;
extern NSInteger const k5DaysInSeconds;
extern NSInteger const k1WeekInSeconds;
extern NSInteger const k10DaysInSeconds;
extern NSInteger const k2WeeksInSeconds;
extern NSInteger const k6MonthsInSeconds;

@interface NSDate (Constants)

@end
