//
//  NSDate+Constants.m
//  Near Lock
//
//  Created by Filip Duvnjak on 5/14/16.
//  Copyright © 2016 Oltica. All rights reserved.
//

#import "NSDate+Constants.h"

NSInteger const k1MinuteInSeconds = 60;
NSInteger const k3MinutesInSeconds = 180;
NSInteger const k5MinutesInSeconds = 300;
NSInteger const k10MinutesInSeconds = 600;
NSInteger const k15MinutesInSeconds = 900;
NSInteger const k30MinutesInSeconds = 1800;
NSInteger const k1HourInSeconds = 3600;
NSInteger const k1DayInSeconds = 86400;
NSInteger const k2DaysInSeconds = 172800;
NSInteger const k5DaysInSeconds = 432000;
NSInteger const k1WeekInSeconds = 604800;
NSInteger const k10DaysInSeconds = 864000;
NSInteger const k2WeeksInSeconds = 1209600;
NSInteger const k6MonthsInSeconds = 12960000;

@implementation NSDate (Constants)

@end
