//
//  NSDateAddition.m
//  mobileclient
//
//  Created by Tobias Konsek on 10/8/11.
//  Copyright 2011 salesperformer.net. All rights reserved.
//

#import "NSDate+Calculations.h"
#import "NSCalendar+CachedCalendar.h"
#import "NSDateFormatter+CachedFormatters.h"

static NSDateComponents *minuteOffset;
static NSDateComponents *hourOffset;
static NSDateComponents *dayOffset;
static NSDateComponents *weekOffset;
static NSDateComponents *monthOffset;
static NSDateComponents *yearOffset;

static NSDateComponents *tomorrowOffset;
static NSDateComponents *yesterdayOffset;

static NSDateComponents *previousMonthOffset;
static NSDateComponents *nextMonthOffset;

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)

@implementation NSDate(Calculations)

- (BOOL) isTodayInWeekWithStart:(NSInteger)day andMonth:(NSInteger)month andYear:(NSInteger)year
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    
    NSDate *date = [NSDate date];
    
    NSDateComponents *dateBuild = [[NSDateComponents alloc] init];
    [dateBuild setDay:day];
    [dateBuild setMonth:month];
    [dateBuild setYear:year];
    
    NSDate *toCheck = [calendar dateFromComponents:dateBuild];
    
    NSDateComponents *comps = [calendar components:(kCFCalendarUnitDay)|(kCFCalendarUnitMonth)|(kCFCalendarUnitYear) fromDate:date];
    NSDateComponents *offset = [[NSDateComponents alloc] init];
    [offset setDay:1];
    for (int i = 0; i < 7; i++) {
        
        if (([comps day] == day) && ([comps month] == month) && ([comps year] == year)) {
            return YES;
        }
        
        toCheck = [calendar dateByAddingComponents:offset toDate:toCheck options:0];
        comps = [calendar components:(kCFCalendarUnitDay)|(kCFCalendarUnitMonth)|(kCFCalendarUnitYear) fromDate:toCheck];
        
        day = [comps day];
        month = [comps month];
        year = [comps year];
        
        comps = [calendar components:(kCFCalendarUnitDay)|(kCFCalendarUnitMonth)|(kCFCalendarUnitYear) fromDate:date];
    }
    
    return NO;
}

//Monday is 2
- (NSDate *) dateWithWeekDay:(NSInteger)weekDay;
{
    NSCalendar *current = [NSCalendar currentCalendarCached];
    NSDateComponents *nowComponents = [current components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:self];
    [nowComponents setWeekday:weekDay];
    return [current dateFromComponents:nowComponents];
}

- (NSDate*)getTomorrow
{
    if (!tomorrowOffset) {
        tomorrowOffset = [[NSDateComponents alloc] init];
        [tomorrowOffset setDay:1];
    }
    
    return [[NSCalendar currentCalendarCached] dateByAddingComponents:tomorrowOffset toDate:self options:0];
}

- (NSDate*)getYesterday
{
    if (!yesterdayOffset) {
        yesterdayOffset = [[NSDateComponents alloc] init];
        [yesterdayOffset setDay:-1];
    }
    NSCalendar *current = [NSCalendar currentCalendarCached];
    return [current dateByAddingComponents:yesterdayOffset toDate:self options:0];
}

- (NSInteger) getDay
{
    return [[[NSCalendar currentCalendarCached] components:NSDayCalendarUnit fromDate:self] day];
}

- (NSInteger) getMonth
{
    return [[[NSCalendar currentCalendarCached] components:NSMonthCalendarUnit fromDate:self] month];
}

- (NSInteger) getYear
{
    return [[[NSCalendar currentCalendarCached] components:NSYearCalendarUnit fromDate:self] year];
}

- (NSInteger) getHour
{
    return [[[NSCalendar currentCalendarCached] components:NSHourCalendarUnit fromDate:self] hour];
}

//Sunday == 1 to Saturday == 7
- (NSInteger) getWeekday
{
    return [[[NSCalendar currentCalendarCached] components:NSWeekdayCalendarUnit fromDate:self] weekday];
}

//Monday == 1 to Sunday == 7
- (NSInteger) oneBasedWeekday
{
    return [NSDate oneBasedWeekday:[self getWeekday]];
}

//Monday == 1 to Sunday == 7
+ (NSInteger) oneBasedWeekday:(NSInteger)weekday
{
    if (weekday == 1) {
        return 7;
    }
    return weekday - 1;
}

- (NSInteger) getMinute
{
    return [[[NSCalendar currentCalendarCached] components:NSMinuteCalendarUnit fromDate:self] minute];
}

- (NSDate*) startOfDay;
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:self];
    return [calendar dateFromComponents:components];
}

- (NSDate *) startOfMonth;
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit fromDate:self];
    return [calendar dateFromComponents:components];
}

- (NSDate *) startOfYear;
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:self];
    return [calendar dateFromComponents:components];
}

- (NSDate*) endOfDay;
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    return [calendar dateFromComponents:components];
}

- (NSDate *) setDayOfMonth:(NSInteger)dayOfMonth;
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:[NSDate date]];
    [dateComponents setDay:dayOfMonth];
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate *) setMonth:(NSInteger)month;
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:[NSDate date]];
    [dateComponents setMonth:month];
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate*) findMondayOfCurrentWeek
{
    NSInteger weekDay = [self getWeekday];
    NSDate *result = [self copy];
    
    if (weekDay == 1) {
        //It's a sunday
        weekDay = 8;
    }
    while (weekDay > 2) {
        result = [result getYesterday];
        weekDay = [result getWeekday];
    }
    return result;
}

- (NSDate *) getHourWithOffset:(NSInteger)offset;
{
    @synchronized(@"hourOffset"){
        if (!hourOffset) {
            /* better performance 30% */
            hourOffset = [[NSDateComponents alloc] init];
        }
        [hourOffset setHour:offset];
        NSDate *date = [[NSCalendar currentCalendarCached] dateByAddingComponents:hourOffset toDate:self options:0];
        return date;
    }
}

- (NSDate *) getMinuteWithOffset:(NSInteger)offset;
{
    @synchronized(@"minuteOffset"){
        if (!minuteOffset) {
            /* better performance 30% */
            minuteOffset = [[NSDateComponents alloc] init];
        }
        [minuteOffset setMinute:offset];
        NSDate *date = [[NSCalendar currentCalendarCached] dateByAddingComponents:minuteOffset toDate:self options:0];
        return date;
    }
}

- (NSDate*) getDayWithOffset:(NSInteger)offset
{
    @synchronized(@"dayOffset"){
        if (!dayOffset) {
            /* better performance 30% */
            dayOffset = [[NSDateComponents alloc] init];
        }
        [dayOffset setDay:offset];
        NSDate *date = [[NSCalendar currentCalendarCached] dateByAddingComponents:dayOffset toDate:self options:0];
        
        return date;
    }
}

- (NSDate*) getWeekWithOffset:(NSInteger)offset;
{
    @synchronized(@"weekOffset"){
        if (!weekOffset) {
            /* better performance 30% */
            weekOffset = [[NSDateComponents alloc] init];
        }
        [weekOffset setWeek:offset];
        NSDate *date = [[NSCalendar currentCalendarCached] dateByAddingComponents:weekOffset toDate:self options:0];
        
        return date;
    }
}

- (NSDate*) getMonthWithOffset:(NSInteger)offset;
{
    @synchronized(@"monthOffset"){
        if (!monthOffset) {
            /* better performance 30% */
            monthOffset = [[NSDateComponents alloc] init];
        }
        [monthOffset setMonth:offset];
        NSDate *date = [[NSCalendar currentCalendarCached] dateByAddingComponents:monthOffset toDate:self options:0];
        return date;
    }
}

- (NSDate*) getYearWithOffset:(NSInteger)offset;
{
    @synchronized(@"yearOffset"){
        if (!yearOffset) {
            /* better performance 30% */
            yearOffset = [[NSDateComponents alloc] init];
        }
        [yearOffset setYear:offset];
        NSDate *date = [[NSCalendar currentCalendarCached] dateByAddingComponents:yearOffset toDate:self options:0];
        
        return date;
    }
}

+ (NSDate*) dateFromDay:(NSInteger)day andMonth:(NSInteger)month andYear:(NSInteger)year
{
    NSCalendar *cal = [NSCalendar currentCalendarCached];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:day];
    [comps setMonth:month];
    [comps setYear:year];
    
    NSDate* date = [cal dateFromComponents:comps];
    
    return date;
}

- (NSDate*) getPreviousMonth
{
    if (!previousMonthOffset) {
        previousMonthOffset = [[NSDateComponents alloc] init];
        [previousMonthOffset setMonth:-1];
    }
    
    return [[NSCalendar currentCalendarCached] dateByAddingComponents:previousMonthOffset toDate:self options:0];
}

- (NSDate*) getNextMonth
{
    if (!nextMonthOffset) {
        nextMonthOffset = [[NSDateComponents alloc] init];
        [nextMonthOffset setMonth:1];
    }
    
    return [[NSCalendar currentCalendarCached] dateByAddingComponents:nextMonthOffset toDate:self options:0];
}

# pragma mark - NEW AND FINE

//+ (int)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
//{
//    NSDate *fromDate;
//    NSDate *toDate;
//
//    NSCalendar *calendar = [NSCalendar currentCalendarCached];
//
//    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate interval:NULL forDate:fromDateTime];
//    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate interval:NULL forDate:toDateTime];
//
//    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
//                                               fromDate:fromDate toDate:toDate options:0];
//
//    return [difference day];
//}

/* seems to be pretty much faster then the above method*/
+ (NSInteger)daysBetweenDate:(NSDate *)startDate andDate:(NSDate *)endDate
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit fromDate:startDate toDate:endDate options:0];
    NSInteger numDays = [components day];
    return numDays;
}

+ (NSInteger) minutesBetweenDates:(NSDate *)firstDate andDate:(NSDate *)secondDate
{
    return fabs([firstDate timeIntervalSince1970] - [secondDate timeIntervalSince1970])/60;
}

/* Midnights between two dates */
+ (NSInteger) midnightsBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
{
    NSDate* sourceDate = [NSDate date];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger timeZoneOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSInteger startDay = [calendar ordinalityOfUnit:NSDayCalendarUnit
                                             inUnit:NSEraCalendarUnit
                                            forDate:[fromDateTime dateByAddingTimeInterval:timeZoneOffset]];
    
    NSInteger endDay = [calendar ordinalityOfUnit:NSDayCalendarUnit
                                           inUnit:NSEraCalendarUnit
                                          forDate:[toDateTime dateByAddingTimeInterval:timeZoneOffset]];
    return endDay - startDay;
}

- (NSDate*) dateWithHour:(NSInteger)hour andMinuten:(NSInteger)minute
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:self];
    [components setHour:hour];
    [components setMinute:minute];
    return [calendar dateFromComponents:components];
}

- (NSDate*) dateWithHour:(NSInteger)hour
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:self];
    [components setHour:hour];
    return [calendar dateFromComponents:components];
}

- (NSDate*) dateWithMinute:(NSInteger)minute
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:self];
    [components setMinute:minute];
    return [calendar dateFromComponents:components];
}

- (NSInteger) numberOfDaysInMonth;
{
    NSRange dayRange = [[NSCalendar currentCalendarCached]
                        rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:self];
    return dayRange.length;
}

- (BOOL)isToday
{
    NSCalendar *cal = [NSCalendar currentCalendarCached];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:self];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:otherDate];
}

- (BOOL) isSameYearAs:(NSDate*)date;
{
    NSDateComponents *otherMonth = [[NSCalendar currentCalendarCached] components:NSEraCalendarUnit|NSYearCalendarUnit fromDate:date];
    NSDateComponents *thisMonth = [[NSCalendar currentCalendarCached] components:NSEraCalendarUnit|NSYearCalendarUnit fromDate:self];
    if([otherMonth year] == [thisMonth year]) {
        return YES;
    }
    return NO;
}

- (BOOL)isSameMonthAs:(NSDate*)date
{
    NSDateComponents *otherMonth = [[NSCalendar currentCalendarCached] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit fromDate:date];
    NSDateComponents *thisMonth = [[NSCalendar currentCalendarCached] components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit fromDate:self];
    if([otherMonth month] == [thisMonth month] &&
       [otherMonth year] == [thisMonth year]) {
        return YES;
    }
    return NO;
}


- (BOOL)isSameDayAs:(NSDate*)date
{
    unsigned int flags = NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit;
    
    NSDateComponents *otherDay = [[NSCalendar currentCalendarCached] components:flags fromDate:date];
    NSDateComponents *thisDay = [[NSCalendar currentCalendarCached] components:flags fromDate:self];
    
    if([thisDay day] == [otherDay day] &&
       [thisDay month] == [otherDay month] &&
       [thisDay year] == [otherDay year] &&
       [thisDay era] == [otherDay era]) {
        return YES;
    }
    return NO;
    
    /* slower */
    //    NSDateFormatter *dateFormatter = [NSDateFormatter dictionaryDateFormatter];
    //    NSString *_d1 = [dateFormatter stringFromDate:self];
    //    NSString *_d2 = [dateFormatter stringFromDate:date];
    //    return [_d1 isEqualToString:_d2];
    
    /* slower */
    //    if ([[[NSCalendar currentCalendarCached] dateFromComponents:otherDay] isEqualToDate:[[NSCalendar currentCalendarCached] dateFromComponents:thisDay]]) {
    //        return YES;
    //    }
    //    return NO;
}

+ (NSDate*) earlierDateOf:(NSDate*)date andDate:(NSDate*)dateTwo
{
    NSComparisonResult result = [date compare:dateTwo];
    
    if (result == NSOrderedAscending) {
        return date;
    }
    
    return dateTwo;
}

+ (NSDate*) laterDateOf:(NSDate*)date andDate:(NSDate*)dateTwo
{
    NSComparisonResult result = [date compare:dateTwo];
    
    if (result == NSOrderedAscending) {
        return dateTwo;
    }
    
    return date;
}

- (NSInteger) getTime:(NSDateKindOfTime)kind
{
    NSCalendar *calendar = [NSCalendar currentCalendarCached];
    NSInteger result;
    
    NSDateComponents *components = nil;
    
    switch (kind) {
        case Month:
            components = [calendar components:NSCalendarUnitMonth fromDate:self];
            result = [components month];
            break;
            
        case Day:
            components = [calendar components:NSCalendarUnitDay fromDate:self];
            result = [components day];
            break;
            
        case Year:
            components = [calendar components:NSCalendarUnitYear fromDate:self];
            result = [components year];
            break;
            
        case Hour:
            components = [calendar components:NSCalendarUnitHour fromDate:self];
            result = [components hour];
            break;
            
        case Minute:
            components = [calendar components:NSCalendarUnitMinute fromDate:self];
            result = [components minute];
            break;
            
        default:
            break;
    }
    
    return result;
}

- (BOOL)isBetween:(NSDate*)date andDate:(NSDate*)otherDate
{
    return (([self earlierDate:otherDate] == self)&& ([self laterDate:date] == self));
}

- (BOOL) isStartOfDay
{
    return ([self getTime:Hour] == 0) && ([self getTime:Minute] == 0);
}

- (BOOL) isEndOfDay
{
    return ([self getTime:Hour] == 23) && ([self getTime:Minute] == 59);
}

-(BOOL)isBeforeDate:(NSDate *)date
{
    if ([self compare:date] == NSOrderedAscending) {
        return YES;
    }
    return NO;
}

-(BOOL)isAfterDate:(NSDate *)date
{
    if ([self compare:date] == NSOrderedDescending) {
        return YES;
    }
    return NO;
}

@end
