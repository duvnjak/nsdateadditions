//
//  NSDateFormatter+CachedFormatters.m
//  mobileclient
//
//  Created by Filip Duvnjak on 5/3/14.
//  Copyright (c) 2014 salesperformer.net. All rights reserved.
//

#import "NSDateFormats.h"
#import "NSDateFormatter+CachedFormatters.h"

static NSDateFormatter *dictionaryDateFormatter;
static NSDateFormatter *dictionaryYearFormatter;
static NSDateFormatter *dictionaryMonthFormatter;
static NSDateFormatter *dictionaryDayFormatter;

static NSDateFormatter *eventDateFormatter;
static NSDateFormatter *localFormatter;

static NSDateFormatter *fullMonthNameYearFormatter;
static NSDateFormatter *fullMonthNameFormatter;

static NSMutableDictionary *dateFormatterDictionary;

@implementation NSDateFormatter (CachedFormatters)

+ (NSDateFormatter *)dateFormatterForDateFormat:(NSString *)dateFormat {
  if (!dateFormatterDictionary) {
    dateFormatterDictionary = [[NSMutableDictionary alloc] init];
  }

  NSDateFormatter *dateFormatter =
      [dateFormatterDictionary objectForKey:dateFormat];
  if (!dateFormatter) {
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    [dateFormatterDictionary setObject:dateFormatter forKey:dateFormat];
  }
  return dateFormatter;
}

+ (NSDateFormatter *)dictionaryDateFormatter;
{
  if (!dictionaryDateFormatter) {
    dictionaryDateFormatter = [[NSDateFormatter alloc] init];
    [dictionaryDateFormatter setDateFormat:DICTIONARY_DATE_FORMAT];
  }
  return dictionaryDateFormatter;
}

+ (NSDateFormatter *)dictionaryYearFormatter;
{
  if (!dictionaryYearFormatter) {
    dictionaryYearFormatter = [[NSDateFormatter alloc] init];
    [dictionaryYearFormatter setDateFormat:DICTIONARY_YEAR_FORMAT];
  }
  return dictionaryYearFormatter;
}

+ (NSDateFormatter *)dictionaryMonthFormatter;
{
  if (!dictionaryMonthFormatter) {
    dictionaryMonthFormatter = [[NSDateFormatter alloc] init];
    [dictionaryMonthFormatter setDateFormat:DICTIONARY_MONTH_FORMAT];
  }
  return dictionaryMonthFormatter;
}

+ (NSDateFormatter *)dictionaryDayFormatter;
{
  if (!dictionaryDayFormatter) {
    dictionaryDayFormatter = [[NSDateFormatter alloc] init];
    [dictionaryDayFormatter setDateFormat:DICTIONARY_DAY_FORMAT];
  }
  return dictionaryDayFormatter;
}

+ (NSDateFormatter *)eventDateFormatter;
{
  if (!eventDateFormatter) {
    eventDateFormatter = [[NSDateFormatter alloc] init];
    [eventDateFormatter setDateStyle:NSDateFormatterNoStyle];
    [eventDateFormatter setTimeStyle:NSDateFormatterShortStyle];
  }
  return eventDateFormatter;
}

+ (NSDateFormatter *)localDateFormatter;
{
  if (!localFormatter) {
    localFormatter = [[NSDateFormatter alloc] init];
    [localFormatter setLocale:[NSLocale currentLocale]];
  }
  return localFormatter;
}

+ (NSDateFormatter *)fullMonthNameYearFormatter;
{
  if (!fullMonthNameYearFormatter) {
    fullMonthNameYearFormatter = [[NSDateFormatter alloc] init];
    [fullMonthNameYearFormatter setDateFormat:FULL_MONT_NAME_YEAR_FORMAT];
  }
  return fullMonthNameYearFormatter;
}

+ (NSDateFormatter *)fullMonthNameFormatter;
{
  if (!fullMonthNameFormatter) {
    fullMonthNameFormatter = [[NSDateFormatter alloc] init];
    [fullMonthNameFormatter setDateFormat:FULL_MONT_NAME_FORMAT];
  }
  return fullMonthNameFormatter;
}

@end
