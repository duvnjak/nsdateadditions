//
//  NSDateFormatter+CachedFormatters.h
//  mobileclient
//
//  Created by Filip Duvnjak on 5/3/14.
//  Copyright (c) 2014 salesperformer.net. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (CachedFormatters)

+ (NSDateFormatter *)dateFormatterForDateFormat:(NSString *)dateFormat;

+ (NSDateFormatter *)eventDateFormatter;

+ (NSDateFormatter *)localDateFormatter;

+ (NSDateFormatter *)dictionaryDateFormatter;
+ (NSDateFormatter *)dictionaryYearFormatter;
+ (NSDateFormatter *)dictionaryMonthFormatter;
+ (NSDateFormatter *)dictionaryDayFormatter;

+ (NSDateFormatter *)fullMonthNameYearFormatter;
+ (NSDateFormatter *)fullMonthNameFormatter;

@end
