Pod::Spec.new do |s|
  s.name             = "NSDateAdditions"
  s.version          = "1.0"
  s.summary          = "The open source fonts for NSDate categories."
  s.homepage         = "https://bitbucket.org/duvnjak/nsdateadditions.git"
  s.license          = 'Code is MIT, then custom font licenses.'
  s.author           = { "duvnjak99" => "filip.duvnjak@hotmail.com" }
  s.source           = { :git => "https://bitbucket.org/duvnjak/nsdateadditions.git", :branch => "master", :tag => s.version.to_s }
  s.social_media_url = ''

  s.ios.platform = :ios, "5.0"
  s.osx.platform = :osx, "10.7"
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  s.resources = 'Pod/Assets/*'

  s.module_name = 'NSDateAdditions'
end